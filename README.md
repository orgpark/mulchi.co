# To Run

- nvm use 16
- npm run start

# Tech Stack

## UI

- Material UI
- Tailwind CSS

## Testing

- Jest
- Cypress

## CI/CD

- GitLab
