console.log('palabras');

const fs = require('fs');

let outputJson = {};

let fileContent = fs.readFileSync('./resources/palabras-future.csv', 'utf-8');
let lines = fileContent.split(/\r?\n/);

// Process each line
lines.forEach((line, index) => {
  const parts = line.split(',');
  const verbs = parts.map((part) => part.trim());
  outputJson[verbs[0]] = {
    future: {
      yo: verbs[1],
      tu: verbs[2],
      el: verbs[3],
      nosotros: verbs[4],
      vosotros: verbs[5],
      ellos: verbs[6],
    },
  };
});

fileContent = fs.readFileSync('./resources/palabras-imperfect.csv', 'utf-8');
lines = fileContent.split(/\r?\n/);

// Process each line
lines.forEach((line, index) => {
  const parts = line.split(',');
  const verbs = parts.map((part) => part.trim());
  outputJson[verbs[0]] = {
    ...outputJson[verbs[0]],
    imperfect: {
      yo: verbs[1],
      tu: verbs[2],
      el: verbs[3],
      nosotros: verbs[4],
      vosotros: verbs[5],
      ellos: verbs[6],
    },
  };
});

fileContent = fs.readFileSync('./resources/palabras-present.csv', 'utf-8');
lines = fileContent.split(/\r?\n/);

// Process each line
lines.forEach((line, index) => {
  const parts = line.split(',');
  const verbs = parts.map((part) => part.trim());
  outputJson[verbs[0]] = {
    ...outputJson[verbs[0]],
    present: {
      yo: verbs[1],
      tu: verbs[2],
      el: verbs[3],
      nosotros: verbs[4],
      vosotros: verbs[5],
      ellos: verbs[6],
    },
  };
});

fileContent = fs.readFileSync('./resources/palabras-preterite.csv', 'utf-8');
lines = fileContent.split(/\r?\n/);

// Process each line
lines.forEach((line, index) => {
  const parts = line.split(',');
  const verbs = parts.map((part) => part.trim());
  outputJson[verbs[0]] = {
    ...outputJson[verbs[0]],
    preterite: {
      yo: verbs[1],
      tu: verbs[2],
      el: verbs[3],
      nosotros: verbs[4],
      vosotros: verbs[5],
      ellos: verbs[6],
    },
  };
});

fileContent = fs.readFileSync('./resources/palabras-subjunctive.csv', 'utf-8');
lines = fileContent.split(/\r?\n/);

// Process each line
lines.forEach((line, index) => {
  const parts = line.split(',');
  const verbs = parts.map((part) => part.trim());
  outputJson[verbs[0]] = {
    ...outputJson[verbs[0]],
    subjunctive: {
      yo: verbs[1],
      tu: verbs[2],
      el: verbs[3],
      nosotros: verbs[4],
      vosotros: verbs[5],
      ellos: verbs[6],
    },
  };
});

console.log('outputJson: ', JSON.stringify(outputJson));
