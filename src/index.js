import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga4';

import './index.css';
import App from './App';

ReactGA.initialize('G-W0Z3CFZ5GS');

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
