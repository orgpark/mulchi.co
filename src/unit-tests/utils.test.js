const { replaceHourMinString, compareWithoutAccents } = require('../utils');

describe('test utils.js file', () => {
  it('converts 2022-03-11T20:10:31-08:00 to 2022-03-11T13:24:00-08:00', () => {
    let result = replaceHourMinString('2022-03-11T20:10:31-08:00', '13:24');
    expect(result).toBe('2022-03-11T13:24:00-08:00');
  });
  it('converts 2022-03-11T20:10:31-08:00 to 2022-03-11T07:24:00-08:00', () => {
    let result = replaceHourMinString('2022-03-11T20:10:31-08:00', '07:24');
    expect(result).toBe('2022-03-11T07:24:00-08:00');
  });
  it('converts 2022-03-11T20:10:31-08:00 to 2022-03-11T07:24:00-08:00', () => {
    let result = replaceHourMinString('2022-03-11T20:10:31-08:00', '7:24');
    expect(result).toBe('2022-03-11T07:24:00-08:00');
  });
});
